/******************************************************************************/
/*                                                                            */
/*   Furgalladas                                                              */
/*   Project: LaunchWarningSystem
/*                                                                            */
/*   This code is released under MIT license                                  */
/*                                                                            */
/******************************************************************************/


/** \file
  airRaidSiren.ino - code for dont wake me project (useless machine)


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> LaunchWarningSystem.ino          </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                              </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                         </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho                  </td></tr>
   <tr> <td> Date:     </td> <td> 21-September-2020 22:25:56        </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> LaunchWarningSystem </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2020] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include <Servo.h>
#include <EEPROM.h>
#include "LaunchWarningSystem.h"


void setup() {
  // put your setup code here, to run once:
  pinMode(ESC_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  
  pinMode(TRIGGER_PIN, INPUT);
  pinMode(EXT_TRIGGER_PIN, INPUT);
  pinMode(STORE_PIN, INPUT);
  pinMode(MODE_PIN, INPUT);

  esc.attach(ESC_PIN);  // attaches the servo on pin 9 to the servo object
  //Serial.begin(9600);
  /* Init routine for esc */
  rearmEsc(); 
}

void loop() {
  /* MANUAL MODE */
  if (digitalRead(MODE_PIN)){
    readSpeed = analogRead(POT_PIN);  // read the input pin
    //Serial.println(readSpeed);          // debug value
    //currentSpeed = (unsigned int)readSpeed*0.15;
    currentSpeed = map(readSpeed, 0, 1023, MIN_SPEED, 180);
    if (firstTimeProtection){
      //Serial.println(currentSpeed);
      esc.write(currentSpeed);
      if (digitalRead(STORE_PIN)){
        //Serial.println("store pressed");
        /*Store current speed into EEPROM */
        writeIntIntoEEPROM(speedAddress, currentSpeed);
      }
    }else{
      if (currentSpeed < MIN_SPEED_THRESHOLD){
        firstTimeProtection = true;
      }
    }
    /* Rearm feature */
    if (digitalRead(TRIGGER_PIN)){
      if (rearmFlag){
        if (millis()-rearmTimeStamp>REARM_TIMER){
          /* rearming... */
          //Serial.println("Rearming...");
          rearmEsc();
          rearmFlag=false; 
        }else{
          /* Just wait */
          //Serial.println("waitign for rearm");
        }
      }else{
        rearmTimeStamp = millis();
        rearmFlag=true;
      }
      
    }else{
      rearmFlag=false;
    }   
  }
  else
  {   /* AUTOMATIC MODE */ 
    //Serial.println("AM");
    /* Reset speed */
    esc.write(MIN_SPEED);
    firstTimeProtection = false;
    if (digitalRead(TRIGGER_PIN)){
      //Serial.println("trigger pressed");
      //Serial.print("Eeprom speed is: ");
      //Serial.println(eepromSpeed);
      eepromSpeed=getSpeedFromEeprom();
      delayValue = TIME_RISING / eepromSpeed;
      for (currentSpeed = MIN_SPEED; currentSpeed<eepromSpeed; currentSpeed++){
        //Serial.println("cs");Serial.println(currentSpeed);
        esc.write(currentSpeed);
        delay (delayValue);
      }
      //Serial.println("max speed reached");
      esc.write(currentSpeed);
      delay(TIME_UP);
      delayValue = TIME_DOWN / eepromSpeed;
      for (currentSpeed = eepromSpeed; currentSpeed>MIN_SPEED; currentSpeed--){
        esc.write(currentSpeed);
        //xSerial.println("cs");Serial.println(currentSpeed);
        delay (delayValue);
      }
      currentSpeed=MIN_SPEED;
      esc.write(currentSpeed);
    }
    if (digitalRead(EXT_TRIGGER_PIN)){
      //Serial.println("trigger pressed");
      //Serial.print("Eeprom speed is: ");
      //Serial.println(eepromSpeed);
      eepromSpeed=getSpeedFromEeprom();
      delayValue = TIME_RISING / eepromSpeed;
      for (currentSpeed = MIN_SPEED; currentSpeed<eepromSpeed; currentSpeed++){
        //Serial.println("cs");Serial.println(currentSpeed);
        esc.write(currentSpeed);
        delay (delayValue);
      }
      //Serial.println("max speed reached");
      esc.write(currentSpeed);
      while (digitalRead(EXT_TRIGGER_PIN)!=LOW){
        //Do nothing
      }
      delayValue = TIME_DOWN / eepromSpeed;
      for (currentSpeed = eepromSpeed; currentSpeed>MIN_SPEED; currentSpeed--){
        esc.write(currentSpeed);
        //xSerial.println("cs");Serial.println(currentSpeed);
        delay (delayValue);
      }
      currentSpeed=MIN_SPEED;
      esc.write(currentSpeed);
    }
  }
  delay(105);
}
