/******************************************************************************/
/*                                                                            */
/*   Furgalladas                                                              */
/*   Project: LaunchWarningSystem
/*                                                                            */
/*   This code is released under MIT license                                  */
/*                                                                            */
/******************************************************************************/


/** \file
  airRaidSiren.ino - code for dont wake me project (useless machine)


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> LaunchWarningSystem.ino          </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                              </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                         </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho                  </td></tr>
   <tr> <td> Date:     </td> <td> 21-September-2020 22:25:56        </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> LaunchWarningSystem </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2020] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/*Pin definitions */
#define LED_PIN           12u
#define ESC_PIN           6u
#define TRIGGER_PIN       8u
#define EXT_TRIGGER_PIN   9u
#define STORE_PIN         2u
#define MODE_PIN          3u
#define POT_PIN           A0

/* Configurations */
#define TIME_UP            5000
#define TIME_DOWN          5000
#define TIME_RISING        5000
#define MIN_SPEED_THRESHOLD 15
#define REARM_TIMER        3000u
#define MIN_SPEED          12u

/* Function prototypes */
unsigned int getSpeedFromEeprom(void);
void rearmEsc(void);
void writeIntIntoEEPROM(int address, int number);
unsigned int readIntFromEEPROM(int address); 


/* Rearm variables */
unsigned long int rearmTimeStamp;
bool rearmFlag = false;

/* Global variables */
Servo esc;  // create servo object to control a servo
int speedAddress=0x0000;
unsigned int currentSpeed = 0;    // variable to store the servo position
unsigned int eepromSpeed; 
unsigned int readSpeed = 0;
int delayValue;
bool firstTimeProtection = false; 


/********************************************************/
/**************** AUXILIAR FUNCTIONS ********************/
/********************************************************/

unsigned int getSpeedFromEeprom(){
  return readIntFromEEPROM(speedAddress);
}


void writeIntIntoEEPROM(int address, unsigned int number)
{ 
  byte byte1 = number >> 8;
  byte byte2 = number & 0xFF;
  EEPROM.write(address, byte1);
  EEPROM.write(address + 1, byte2);
}

unsigned int readIntFromEEPROM(int address)
{
  byte byte1 = EEPROM.read(address);
  byte byte2 = EEPROM.read(address + 1);
  return (byte1 << 8) + byte2;
}

void rearmEsc(){
  digitalWrite(LED_PIN,LOW);
  esc.writeMicroseconds(2000);
  delay (5000);
  digitalWrite(LED_PIN,HIGH);
  esc.write (7);
}
